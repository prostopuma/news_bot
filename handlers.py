from main import bot, dp
from aiogram.types import Message
from config import admin_id
from pars import kurs, news, pogoda
from data import add_message, users, bot_answer

async def send_to_admin(dp):
    await bot.send_message(chat_id=admin_id, text="Бот активен")


# обработчик команды /start
@dp.message_handler(commands=['start'])
async def send_welcome(message: Message):
    # Записываем нового пользователя
    users(user_id=message.chat.id, username=message.chat.username, first_name=message.chat.first_name,
          last_name=message.chat.last_name)
    # Записываем сообщение пользователя
    add_message(user_id=message.chat.id, username=message.chat.username, text=message.text)
    # Отправляет приветственное сообщение и помощь по боту
    await message.answer(
        "Бот ассистент\n\n"
        "Вот что я могу:\n"
        "Вывести курс валют /kurs\n"
        "Сводка новостей /news\n"
        "Погода сегодня /weather\n"
        "Вся сводка новостей /all")


# обработчик команды /kurs
@dp.message_handler(commands=['kurs'])
async def send_kurs(message: Message):
    # Записываем сообщение пользователя
    add_message(user_id=message.chat.id, username=message.chat.username, text=message.text)
    # Передаем переменной wallet значения который нам возращает функция kurs()
    wallet = kurs()
    # Записываем в БД ответ бота
    bot_answer(user_id=message.chat.id, username=message.chat.username, text=wallet)
    # Функция которая отправляет сообщение пользователю
    await bot.send_message(message.from_user.id, wallet)


# обработчик команды /news
@dp.message_handler(commands=['news'])
async def send_news(message: Message):
    # Записываем сообщение пользователя
    add_message(user_id=message.chat.id, username=message.chat.username, text=message.text)
    # Передаем переменной new кортеж который нам возращает функция news()
    new = news()
    # Записываем в БД ответ бота
    bot_answer(user_id=message.chat.id, username=message.chat.username, text=new)
    # Функция которая отправляет сообщение пользователю
    await bot.send_message(message.from_user.id, new)


# обработчик команды /weather
@dp.message_handler(commands=['weather'])
async def send_news(message: Message):
    # Записываем сообщение пользователя
    add_message(user_id=message.chat.id, username=message.chat.username, text=message.text)
    # Передаем переменной weather кортеж который нам возращает функция pogoda()
    weather = pogoda()
    # Записываем в БД ответ бота
    bot_answer(user_id=message.chat.id, username=message.chat.username, text=weather)
    # Функция которая отправляет сообщение пользователю
    await bot.send_message(message.from_user.id, weather)


@dp.message_handler(commands=['all'])
async def send_morning_news(message: Message):
    weather = pogoda()
    new = news()
    wallet = kurs()
    await bot.send_message(message.from_user.id, f'{weather} \n\n {new} \n\n {wallet}')

# Просто для коммита
