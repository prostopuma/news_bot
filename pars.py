import requests
from bs4 import BeautifulSoup

HEADERS = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36',
    'accept': '*/*'}

URL_KURS = "https://cbr.ru/key-indicators/"
URL_START_YANDEX = "https://yandex.ru/"
URL_YANDEX_WEATHER = "https://yandex.ru/pogoda/moscow/details?via=mf#18"


# Функция в которую нужно передать аргумент, ссылку
def get_html(url):
    # C помощью библиотеки requests делаем запрос по ссылке и
    # получаем в ответ html код и записываем его в переменную req
    req = requests.get(url, HEADERS)
    return req


# Функция которая достает нужные нам значения из html
def kurs():
    # Записываем в переменную soup обект библиотеки BeautifulSoup
    soup = BeautifulSoup(get_html(URL_KURS).text, 'html.parser')
    # В переменную curs  с помощью метеда бибилотеки BeautifulSoup записываем список элементов
    curs = soup.find_all('td', class_='value td-w-4 _bold _end mono-num')
    # С помощью форматирования записываем в переменную new_curs ответ который должен отправить бот
    new_curs = f'ВАЛЮТА СЕГОДНЯ:\n\n$ USD: {curs[0].text}\n\n€ EUR: {curs[1].text}'
    return new_curs


def news():
    soup = BeautifulSoup(get_html(URL_START_YANDEX).text, 'html.parser')
    news = soup.find_all('span', class_='news__item-content')
    new_news = f'СЕЙЧАС В СМИ:\n\n {news[0].text} \n\n {news[1].text} \n\n {news[2].text}\n\n {news[3].text} \n\n {news[4].text}\n\n {news[5].text}\n'
    return new_news


def pogoda():
    soup = BeautifulSoup(get_html(URL_YANDEX_WEATHER).text, 'html.parser')
    weather = soup.findAll('span', class_='temp__value temp__value_with-unit')
    new_weather = f'ТЕМПЕРАТУРА СЕГОДНЯ:\n\n Утром от {weather[0].text} до {weather[1].text}\n Днем от {weather[3].text} до {weather[4].text}\n ' \
                  f'Вечером от {weather[6].text} до {weather[7].text}\n Ночью от {weather[9].text} до {weather[10].text}'
    return new_weather

    # Просто для коммита
